/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 3.3
 */

#include <stdio.h>

int is_prime(int number)
{
	int i;
	if (number < 2)
		return 0;

	for (i = 2; i * i <= number; i++) {
		if (number % i == 0)
			return 0;
	}

	return 1;
}

int main()
{

	int i, upper;

	scanf("%d", &i);
	scanf("%d", &upper);

	/* upper should be greater than the lower limit */
	if (i > upper)
		return 1;

	for (; i <= upper; i++) {
		if (is_prime(i))
			printf("%d\n", i);
	}

	return 0;
}
