/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 3.2
 */

#include <stdio.h>

void check_input(int number, int upper_limit)
{
	if (number != 0 && number < upper_limit)
		printf("Input valid!\n");
	else
		printf("Input invalid!\n");
}

int main()
{
	int a = 0, b = 0, c = 0, d = 0;

	/* Read in a    */
	scanf("%d", &a);
	check_input(a, 10);

	/* Read in b    */
	scanf("%d", &b);
	check_input(b, 14);

	/* Read in c    */
	scanf("%d", &c);
	check_input(c, 6);

	/* Read in d    */
	scanf("%d", &d);
	check_input(d, 22);

	printf("a = %d, b = %d, c = %d, d = %d\n", a, b, c, d);

	return 0;
}
