/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 3.6
 */

#include <stdio.h>
#include <math.h>
#include "abc.h"

static double calculate_discriminant(double a, double b, double c)
{
	return b * b - 4 * a * c;
}

void abc(double a, double b, double c)
{
	double d = calculate_discriminant(a, b, c);

	printf("The roots of %fx^2 + %fx + %f are:\n", a, b, c);

	if (d == 0) {
		printf("x = %f\n", -b / (2 * a));
	} else if (d > 0) {
		double x1 = (-b + sqrt(d)) / (2 * a);
		double x2 = (-b - sqrt(d)) / (2 * a);
		printf("x1 = %f, x2 = %f\n", x1, x2);
	} else {
		double real = -b / (2 * a);
		double imag = sqrt(-d) / (2 * a);
		printf("x1 = %f+%fi, x2 = %f-%fi\n", real, imag, real, imag);
	}
}
