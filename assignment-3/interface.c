/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 3.6
 */

#include <stdio.h>
#include "abc.h"

static double a, b, c;

static void get_parameters()
{
	scanf("%lf%lf%lf", &a, &b, &c);
}

int main()
{
	int times = -1;
	scanf("%d", &times);

	/* can't accept negative number of lines */
	if (times < 0)
		return 1;

	while (times--) {
		get_parameters();
		abc(a, b, c);
	}

	return 0;
}
