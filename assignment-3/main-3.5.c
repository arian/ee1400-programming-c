/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 3.5
 */

#include <stdio.h>

double calculate_average(int number)
{
	static int calls = 0;
	static int total = 0;

	calls++;
	total += number;

	return (double) total / calls;
}

int main(void)
{
	double average;

	while (1) {
		int number;

		scanf("%d", &number);

		if (number == 0)
			break;
		else
			average = calculate_average(number);
	}
	printf("%.1f\n", average);

	return 0;
}
