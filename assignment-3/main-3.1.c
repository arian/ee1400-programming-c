/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 3.1
 */

#include <stdio.h>

int f(int x)
{
	return (x + 1) - 1;
}

int main()
{
	printf("%d\n", f(1));
	return 0;
}

