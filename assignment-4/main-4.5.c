/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 4.5
 */

#include <stdio.h>

int main(void)
{
	int a[5] = { 10, 20, 30, 40, 50 };
	int *ptr = a;

	printf("a[0] has value %d and is stored at address %x\n",
	       a[0], (int) &a[0]);
	printf("a[1] has value %d and is stored at address %x\n",
	       a[1], (int) &a[1]);
	printf("a[2] has value %d and is stored at address %x\n",
	       a[2], (int) &a[2]);
	printf("*a has value %d and is stored at address %x\n",
	       *a, (int) a);
	printf("*(a + 2) has value %d and is stored at address %x\n",
	       *(a + 2), (int) (a + 2));
	printf("*ptr has value %d and is stored at address %x\n",
	       *ptr, (int) ptr);
	printf("*(ptr + 2) has value %d and is stored at address %x\n",
	       *(ptr + 2), (int) (ptr + 2));

	return 0;
}
