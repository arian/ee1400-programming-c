/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 4.7
 */

#include <stdio.h>

int main(void)
{
	char s[7] = "abcdef";
	char *ptr = s;

	printf("s[0] has value %c and is stored at address %x\n",
	       s[0], (int) &s[0]);
	printf("s[1] has value %c and is stored at address %x\n",
	       s[1], (int) &s[1]);
	printf("s[2] has value %c and is stored at address %x\n",
	       s[2], (int) &s[2]);
	printf("s has value %s (%x)\n", s, (int) s);
	printf("s + 2 has value %s (%x)\n", s + 2, (int) (s + 2));
	printf("ptr has value %s (%x)\n", ptr, (int) ptr);
	printf("ptr + 2 has value %s (%x)\n", ptr + 2, (int) (ptr + 2));

	return 0;
}
