/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 4.6
 */

#include <stdio.h>
#include <stdlib.h>

static void swap(double **p, double **q)
{
	double *tmp;
	tmp = *p;
	*p = *q;
	*q = tmp;
}

/**
 * Returns the squared length of the vector.
 * We only use it to compare the lengths with another vector
 * so it is faster not to calculate the square root
 */
static double length_sq(double v[], int n)
{
	double length = 0;
	int i;

	for (i = 0; i < n; i++) {
		length += v[i] * v[i];
	}

	return length;
}

static void bubble(double **w, int dim, int num)
{
	int i, j;

	for (i = 0; i < num - 1; ++i) {
		for (j = num - 1; j > i; --j) {
			if (length_sq(w[j - 1], dim) >
			    length_sq(w[j], dim))
				swap(&w[j - 1], &w[j]);
		}
	}
}

int main()
{
	int dim, num;
	int i, j;
	double **w;

	scanf("%d %d", &dim, &num);
	w = calloc(num, sizeof(double *));
	for (i = 0; i < num; i++) {
		w[i] = calloc(dim, sizeof(double));
		for (j = 0; j < dim; j++) {
			scanf("%le", &w[i][j]);
		}
	}

	bubble(w, dim, num);

	for (i = 0; i < num; i++) {
		for (j = 0; j < dim; j++) {
			printf("%e%c", w[i][j], j == dim - 1 ? '\n' : ' ');
		}
	}

	free(w);

	return 0;
}
