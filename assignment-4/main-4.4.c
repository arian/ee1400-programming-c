/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 4.4
 */

#include <stdio.h>

int main(void)
{
	int a, b;
	int *ptr;

	a = 1;
	b = 2;
	ptr = &a;

	printf("a has value %d and is stored at address %x\n",
	       a, (int) &a);
	printf("b has value %d and is stored at address %x\n",
	       b, (int) &b);
	printf("ptr has value %x and is stored at address %x\n",
	       (int) ptr, (int) &ptr);
	printf("The value of the integer pointed to by ptr is %d\n", *ptr);

	return 0;
}
