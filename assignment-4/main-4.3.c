/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 4.3
 */

#include <stdio.h>

int main()
{

	int n, m, i, j;
	double price[100];
	double numbers[100][100];
	double sum;

	/* read the prices */
	scanf("%d", &n);

	/* not allowed to set more than 100 price values */
	if (n > 100)
		return 1;

	for (i = 0; i < n; i++) {
		scanf("%lf", &price[i]);
	}

	/* read numbers */
	scanf("%d", &m);

	/* not allowed to set more than 100 customers */
	if (m > 100)
		return 1;

	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			scanf("%lf", &numbers[i][j]);
		}
	}

	/* display */

	for (i = 0; i < m; i++) {
		sum = 0;
		for (j = 0; j < n; j++) {
			sum += numbers[i][j] * price[j];
		}
		printf("%.2f\n", sum);
	}

	return 0;
}
