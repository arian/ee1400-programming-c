/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 4.8
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int count_words(char *str, char *find, int flen)
{
	int slen = strlen(str);
	int i;
	int count = 0;

	for (i = 0; i < slen; i++) {
		if (strncmp(find, str + i, flen) == 0)
			count++;
	}

	return count;
}

int main(int argc, char *argv[])
{
	char *find;
	int length;
	char buf[1026];
	int count = 0;

	if (argc < 2) {
		fprintf(stderr, "This program expects an argument\n");
		return 1;
	}

	find = argv[1];
	length = strlen(find);

	while (fgets(buf, 1025, stdin) != NULL && strncmp(buf, "#EOF", 4) != 0) {
		count += count_words(buf, find, length);
	}

	printf("%d\n", count);

	return 0;
}
