/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 2.4
 */

#include <stdio.h>

int main()
{

	int s = 1;
	double a = 1.0e20;
	double b = 1.0e20 + 1.0e4;

	printf("a = %f, b = %f\n", a, b);

	for (a = 1.0e20; a < b; a = a + 1) {
		printf("step %d: a = %f\n", s++, a);
	}

	return 0;
}
