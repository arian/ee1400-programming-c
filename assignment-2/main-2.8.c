/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 2.8
 */

#include <stdio.h>

int main()
{
	int i;

	for (i = 0; i < 10; i++) {
		printf("loop %d\n", i);
	}

	return 0;
}
