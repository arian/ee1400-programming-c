/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 2.12
 */

#include <stdio.h>

int main()
{

	int number;
	int result = 1;
	int i;

	scanf("%d", &number);

	if (number < 2) {
		result = 0;
	} else {
		for (i = 2; i * i <= number; i++) {
			if (number % i == 0) {
				result = 0;
				break;
			}
		}
	}

	if (result) {
		printf("%d is a prime number\n", number);
	} else {
		printf("%d is not a prime number\n", number);
	}

	return 0;
}
