/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 2.9
 */

#include <stdio.h>

int main()
{
	int i = 20;

	while (i >= 0) {
		printf("i = %d\n", i);
		i -= 2;
	}

	return 0;
}
