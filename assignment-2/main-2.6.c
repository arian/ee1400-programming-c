/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 2.6
 */

#include <stdio.h>

int main()
{
	int a = 3;
	int b = 4;

	double c = (double) a / b;

	printf("double c = %f\n", c);

	return 0;
}
