/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 2.11
 */

#include <stdio.h>

int main(void)
{
	while (1) {
		int a;

		scanf("%d", &a);
		printf("a + 2 = %d\n", a + 2);
	}

	// the program will never reach this part of the code.
}
