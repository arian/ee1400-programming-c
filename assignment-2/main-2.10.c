/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 2.10
 */

#include <stdio.h>

int main()
{
	int i = 10;

	while (i) {
		printf("i = %5d\n", i--);
	}

	return 0;
}
