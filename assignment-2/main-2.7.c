/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 2.7
 */

#include <stdio.h>

int main()
{
	int i, j;

	scanf("%d%d", &i, &j);

	if (i != 0 && j / i > 5 && j < 10) {
		printf("Input valid!\n");
	}

	return 0;
}
