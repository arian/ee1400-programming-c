/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 1.3
 */
#include <stdio.h>

int main()
{

	double celsius, kelvin, fahrenheit;

	scanf("%lf", &celsius);

	kelvin = celsius + 273.15;
	fahrenheit = celsius * 1.8 + 32;

	printf("C\tK\tF\n");
	printf("%.2f\t%.2f\t%.2f\n", celsius, kelvin, fahrenheit);

	return 0;
}
