/*
 * Student: Arian Stolwijk
 * Nummer: 4001079
 * Opdracht: 1.4
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{

	double a, r;
	int N;

	double S = 0;
	int n;

	scanf("%lf\t%d\t%lf", &a, &N, &r);

	for (n = 0; n <= N; n++) {
		S += a * pow(r, n);
	}

	printf("%.2f\n", S);

	return 0;
}
